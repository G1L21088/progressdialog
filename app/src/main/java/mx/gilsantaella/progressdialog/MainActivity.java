package mx.gilsantaella.progressdialog;

import android.app.ProgressDialog;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private ProgressDialog progressDialog;
    private Button btnDialog;
    private Handler _handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _handler = new Handler();
        btnDialog = (Button) findViewById(R.id.btnDialog);

        btnDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBarDialog();
            }
        });
    }

    public void progressBarDialog() {
        progressDialog = new ProgressDialog(MainActivity.this);

        progressDialog.setTitle("Please Wait...");
        progressDialog.setMessage("Task in progress...");
        progressDialog.setProgressStyle(progressDialog.STYLE_HORIZONTAL);
        progressDialog.setProgress(0);
        progressDialog.setMax(15);
        progressDialog.show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    while(progressDialog.getProgress() <= progressDialog.getMax()){
                        Thread.sleep(500);

                        _handler.post(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.incrementProgressBy(1);
                                progressDialog.setSecondaryProgress(progressDialog.getProgress()+1);
                            }
                        });
                        if(progressDialog.getProgress() == progressDialog.getMax()){
                            progressDialog.dismiss();
                        }
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
/*
Primera funci'on progress indeterminado
                progressDialog = ProgressDialog.show(MainActivity.this, "Please wait ...",  "Task in progress ...", true);
                progressDialog.setCancelable(true);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            //Do some stuff that take some time...
                            Thread.sleep(3000); // Let's wait for some time
                        } catch (Exception e) {

                        }
                        progressDialog.dismiss();
                    }
                }).start();
 */